# Jiminy 🦗

A library for building asynchronous web servers.

## Design

For details on the design of Jiminy, check out [docs/Design.md](./docs/Design.md)

## Problems to think about

* Can I do something with routes defined like `"partA/partB" / "partC"` so that they work as you might expect?
* Add more matching.
    * Headers, can be required, or optional.
    * Host.
* How do I handle failures for things like body parsing?
    * I'm going to have the same problem as Akka and its rejection handlers
* Should I make sure you handle Content-Types correctly?
* I'm dealing a lot with fully saturated datatypes in the routing layer, how could I handle streams/websockets?
    * Maybe some light paramaterisation of the HttpRequest type which can either be the body? as a string, or some pipe
* There's lots of repetition in the way methods are handled in the routing
* How can I allow the user of Jiminy to configure how responses are converted in to bytes?
    * I think the only way that can work out is for Jiminy to offer up a function that takes the routes, the server
      adapter, and then some extra optional stuff to configure this sort of thing.
    * Actually I wonder if I could also target different async runtimes easily?