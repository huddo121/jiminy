package au.net.michaelhudson.jiminy.data

sealed class Stack<out A> {
    fun pop(): Pair<Option<A>, Stack<A>> = when (this) {
        is NonEmpty -> Pair(Some(value), tail)
        Empty -> Pair(Option.none(), this)
    }

    fun isEmpty(): Boolean = when (this) {
        is NonEmpty -> false
        is Empty -> true
    }

    companion object {
        fun <A> empty(): Stack<A> = Empty

        fun <A> of(iterator: Iterator<A>): Stack<A> {
            tailrec fun inner(i: Iterator<A>, acc: Stack<A>): Stack<A> {
                return if (i.hasNext()) {
                    val n = i.next()
                    inner(i, acc.push(n))
                } else {
                    return acc
                }
            }

            return inner(iterator, empty())
        }
    }
}

data class NonEmpty<A>(val value: A, val tail: Stack<A>) : Stack<A>()
object Empty : Stack<Nothing>()


fun <A> Stack<A>.push(item: A): Stack<A> = NonEmpty(item, this)
