package au.net.michaelhudson.jiminy.http

/**
 * HTTP Methods as defined by <a hef="https://datatracker.ietf.org/doc/html/rfc7231#section-4.1">RFC7231</a>, including
 *   the PATCH method from <a href="https://datatracker.ietf.org/doc/html/rfc5789">RFC5789</a>.
 */
sealed class Method {
    sealed class BodylessMethod: Method()
    object GET : BodylessMethod() {
        override fun toString(): String = "GET"
    }
    object HEAD : BodylessMethod() {
        override fun toString(): String = "HEAD"
    }
    object DELETE : BodylessMethod() {
        override fun toString(): String = "DELETE"
    }
    object CONNECT : BodylessMethod() {
        override fun toString(): String = "CONNECT"
    }
    object OPTIONS : BodylessMethod() {
        override fun toString(): String = "OPTIONS"
    }
    object TRACE : BodylessMethod() {
        override fun toString(): String = "TRACE"
    }

    sealed class BodyMethod: Method()
    object POST : BodyMethod() {
        override fun toString(): String = "POST"
    }
    object PUT : BodyMethod() {
        override fun toString(): String = "PUT"
    }
    object PATCH : BodyMethod() {
        override fun toString(): String = "PATCH"
    }
}
