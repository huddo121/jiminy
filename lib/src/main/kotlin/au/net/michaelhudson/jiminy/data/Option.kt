package au.net.michaelhudson.jiminy.data

sealed class Option<out A> {
    abstract fun <B> fold(onNone: () -> B, onSome: (A) -> B): B

    companion object {
        fun <A> none(): Option<A> = None
    }
}

data class Some<A>(val value: A) : Option<A>() {
    override fun <B> fold(onNone: () -> B, onSome: (A) -> B): B = onSome(value)
}

private object None : Option<Nothing>() {
    override fun <B> fold(onNone: () -> B, onSome: (Nothing) -> B): B = onNone()
}

/**
 * Checks to see whether this option contains a value that is structurally equal to the provided `item`.
 */
fun <A> Option<A>.contains(item: A): Boolean = fold({ false }, { it == item })