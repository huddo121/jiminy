package au.net.michaelhudson.jiminy.http

import au.net.michaelhudson.jiminy.data.*

// TODO: Can I use this to carry around the HList types?
data class Handler<A : HList<*>, B>(val f: suspend (A) -> B)

fun <R> handler(f: suspend () -> R): suspend (NoParams) -> R = { f() }
fun <A, R> handler(f: suspend (A) -> R): suspend (OneParam<A>) -> R = { hlist -> f(hlist.head) }
fun <A, B, R> handler(f: suspend (A, B) -> R): suspend (TwoParams<A, B>) -> R =
    { hlist -> f(hlist.head, hlist.tail.head) }

fun <A, B, C, R> handler(f: suspend (A, B, C) -> R): suspend (ThreeParams<A, B, C>) -> R =
    { hlist -> f(hlist.head, hlist.tail.head, hlist.tail.tail.head) }

fun <A, B, C, D, R> handler(f: suspend (A, B, C, D) -> R): suspend (FourParams<A, B, C, D>) -> R =
    { hlist -> f(hlist.head, hlist.tail.head, hlist.tail.tail.head, hlist.tail.tail.tail.head) }

fun <A, B, C, D, E, R> handler(f: suspend (A, B, C, D, E) -> R): suspend (FiveParams<A, B, C, D, E>) -> R =
    { hlist ->
        f(
            hlist.head,
            hlist.tail.head,
            hlist.tail.tail.head,
            hlist.tail.tail.tail.head,
            hlist.tail.tail.tail.tail.head
        )
    }

// TODO: See if I can fix the associativity problem
fun params(): NoParams = HNil
fun <A> params(a: A): OneParam<A> = justA(a)
fun <A, B> params(a: A, b: B): TwoParams<A, B> = a andA b
fun <A, B, C> params(a: A, b: B, c: C): ThreeParams<A, B, C> = a andA (b andA c)
fun <A, B, C, D> params(a: A, b: B, c: C, d: D): FourParams<A, B, C, D> =
    a andA (b andA (c andA d))

typealias AnyParams = HList<*>
typealias NoParams = HNil
typealias ParamsOf<A, TAIL> = HNode<A, TAIL>
typealias OneParam<A> = HNode<A, HNil>
typealias TwoParams<A, B> = HNode<A, HNode<B, HNil>>
typealias ThreeParams<A, B, C> = HNode<A, HNode<B, HNode<C, HNil>>>
typealias FourParams<A, B, C, D> = HNode<A, HNode<B, HNode<C, HNode<D, HNil>>>>
typealias FiveParams<A, B, C, D, E> = HNode<A, HNode<B, HNode<C, HNode<D, HNode<E, HNil>>>>>
