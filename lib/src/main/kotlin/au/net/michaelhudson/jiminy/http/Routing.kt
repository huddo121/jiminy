package au.net.michaelhudson.jiminy.http

import au.net.michaelhudson.jiminy.data.*
import java.net.URLDecoder

data class ParseError(val msg: String, val foundValue: String)
typealias ParseResult<A> = Either<ParseError, A>
typealias Parser<A> = (String) -> ParseResult<A>

fun <A> parseSuccess(a: A): ParseResult<A> = Either.right(a)
fun <A> parseFailure(msg: String, foundValue: String): ParseResult<A> =
    Either.left(ParseError(msg, foundValue))

/**
 * This file can be split in to some fairly well isolated sectioned. The first section contains [RoutingElement] and its
 *   descendents. These represent the actual structure that is consulted to see if a request should be handled for a
 *   specific route. They form a singly linked list of objects with each carrying some extra bit of functionality.
 *
 * There are a number of DSL types (mostly suffixed with `Bit`), which wrap up every parameter required to create their
 *   equivalent [RoutingElement] type, without the link to the previous element. Linking the elements together is done
 *   using combinators provided by this library, primarily through overloading the `div` method
 */

/**
 * [RoutingElement] is the root class that all that represents all types of routing that can occur when handling a
 *   HTTP request
 */
// TODO: This should capture a return type
sealed class RoutingElement<Params : AnyParams> {
    // MatchResult should also carry a function `suspend () -> HttpResponse` that can then be executed to actually
    //   produce the result
    fun matches(request: HttpRequest): MatchResult<Params> = matches(
        Stack.of(request.uri.rawPath.split("/").map { part -> URLDecoder.decode(part, "UTF-8") }.iterator()),
        ConstantRequestElements.from(request)
    )

    internal abstract fun matches(
        pathSections: Stack<String>,
        requestElements: ConstantRequestElements
    ): MatchResult<Params>
}

/**
 * Some parts of the request remain unchanged throughout the entire matching process.
 */
data class ConstantRequestElements(val method: Method, val body: String?) {
    companion object {
        fun from(request: HttpRequest): ConstantRequestElements = ConstantRequestElements(request.method, request.body)
    }
}

sealed class MatchResult<Params : AnyParams> {
    abstract fun isSuccess(): Boolean
    abstract fun <B> fold(onFailedToMatch: () -> B, onMatched: (Params) -> B): B
}

data class SuccessfulMatch<Params : AnyParams>(val params: Params) : MatchResult<Params>() {
    override fun isSuccess(): Boolean = true
    override fun <B> fold(onFailedToMatch: () -> B, onMatched: (Params) -> B): B = onMatched(params)
}

// TODO: Will probably make this a data class at some point to do something similar to rejections in Akka-Http
class FailedToMatch<Params : AnyParams> : MatchResult<Params>() {
    override fun isSuccess(): Boolean = false
    override fun <B> fold(onFailedToMatch: () -> B, onMatched: (Params) -> B): B = onFailedToMatch()
    override fun equals(other: Any?): Boolean {
        // All match failures are equal today
        return other is FailedToMatch<*>
    }

    override fun hashCode(): Int {
        return javaClass.hashCode()
    }
}

/**
 * The main types that are responsible for being consulted as to whether a HttpRequest should be handled by this route
 */
sealed class PathRouting<A : AnyParams> : RoutingElement<A>()

object PathRoot : PathRouting<NoParams>() {
    override fun matches(
        pathSections: Stack<String>,
        requestElements: ConstantRequestElements
    ): MatchResult<NoParams> {
        val (top, tail) = pathSections.pop()

        return if (top.contains("") && (tail.isEmpty() || tail.pop().first.contains(""))) SuccessfulMatch(NoParams) else FailedToMatch()
    }
}

data class PathPart<A : AnyParams>(val pathPart: String, val prev: PathRouting<A>) : PathRouting<A>() {
    override fun matches(
        pathSections: Stack<String>,
        requestElements: ConstantRequestElements
    ): MatchResult<A> {
        val (top, tail) = pathSections.pop()
        return top.fold(
            { FailedToMatch() },
            { section ->
                if (pathPart == section) prev.matches(tail, requestElements) else FailedToMatch()
            }
        )
    }
}

data class PathParam<P, TAIL : AnyParams>(
    val paramName: String,
    val paramTransformer: Parser<P>,
    val prev: PathRouting<TAIL>
) :
    PathRouting<ParamsOf<P, TAIL>>() {
    override fun matches(
        pathSections: Stack<String>,
        requestElements: ConstantRequestElements
    ): MatchResult<ParamsOf<P, TAIL>> {
        val (top, tail) = pathSections.pop()
        return top.fold(
            { FailedToMatch() },
            { section ->
                paramTransformer(section).fold(
                    { FailedToMatch() },
                    { param ->
                        val result = prev.matches(tail, requestElements)
                        when (result) {
                            is SuccessfulMatch<TAIL> -> {
                                SuccessfulMatch(param andA result.params)
                            }
                            is FailedToMatch -> FailedToMatch()
                        }
                    }
                )
            }
        )
    }
}

// Ugh, this file proves one of the challenges of the way these objects are constructed, things are checked from the
//  end to the start, but for this type of server we don't care so much about the tail of the path, really only care
//  up to some shared root.
data class StaticResourceServer<P : AnyParams>(val basePath: String, val prev: PathRouting<P>) :
    RoutingElement<P>() {

    // Try and find the split between the prefix of the path and the files relative to the basepath.
    private fun prefixPathMatch(
        pathSections: Stack<String>,
        requestElements: ConstantRequestElements
    ): Either<FailedToMatch<P>, SuccessfulMatch<P>> {
        // Probably more wrapping than necessary
        // I was collecting the filepath but can't put it anywhere :hmmm:
        fun go(sections: Stack<String>, consumed: List<String>): Either<FailedToMatch<P>, SuccessfulMatch<P>> {
            val matchesSections = prev.matches(sections, requestElements)

            return when (matchesSections) {
                is SuccessfulMatch -> {
                    println("Got a match for files, with filepath of ${consumed.joinToString("/")}")
                    return Right(matchesSections)
                }
                is FailedToMatch -> {
                    val (top, tail) = sections.pop()

                    top.fold(
                        {
                            // Ran out of things to match with
                            Left(FailedToMatch())
                        },
                        { topSection ->
                            go(tail, listOf(topSection) + consumed)
                        }
                    )
                }
            }
        }

        return go(pathSections, emptyList())
    }

    override fun matches(
        pathSections: Stack<String>,
        requestElements: ConstantRequestElements
    ): MatchResult<P> {
        val (top, tail) = pathSections.pop()
        return top.fold(
            {
                // This should be a success, but probably needs to be some sort of default like index.html?
                println("No top, es bueno?")
                FailedToMatch()
            },
            { section ->
                // We're likely to see a longer path than the one declared for this route
                // But regular match will not handle the trailing path
                // So lets do something a little yucky and just keep trying until we either get a match or run out of
                //  pieces to match with
                val canFindMatch = prefixPathMatch(pathSections, requestElements)

                canFindMatch.fold(
                    { FailedToMatch() },
                    { match ->
                        match
                    }
                )
            }
        )
    }
}

/**
 * The MethodPart types represent routing based on the HTTP Verb used
 */
sealed class MethodPart<A : AnyParams>() : RoutingElement<A>() {
    /**
     * The string that is printed when rendering this element
     */
    abstract val methodName: String
}

/**
 * If you handle all methods, then you need to handle a possible body type, and a response type
 */
data class AllMethods<Tail : AnyParams, Body, DomainIn>(
    val bodyTransformer: (Body?) -> DomainIn,
    val prev: PathRouting<Tail>
) : MethodPart<Tail>() {
    override val methodName: String = "ALL"

    // Given this handles all methods, there's nothing for us to check
    override fun matches(
        pathSections: Stack<String>,
        requestElements: ConstantRequestElements
    ): MatchResult<Tail> = prev.matches(
        pathSections,
        requestElements
    )
}

data class Get<Tail : AnyParams>(
    val prev: PathRouting<Tail>
) : MethodPart<Tail>() {
    override val methodName: String = "GET"

    override fun matches(
        pathSections: Stack<String>,
        requestElements: ConstantRequestElements
    ): MatchResult<Tail> {
        return if (requestElements.method == Method.GET) prev.matches(
            pathSections,
            requestElements
        ) else FailedToMatch()
    }
}

data class Head<Tail : AnyParams>(
    val prev: PathRouting<Tail>
) : MethodPart<Tail>() {
    override val methodName: String = "HEAD"

    override fun matches(
        pathSections: Stack<String>,
        requestElements: ConstantRequestElements
    ): MatchResult<Tail> {
        return if (requestElements.method == Method.HEAD) prev.matches(
            pathSections,
            requestElements
        ) else FailedToMatch()
    }
}

data class Delete<Tail : AnyParams>(
    val prev: PathRouting<Tail>
) : MethodPart<Tail>() {
    override val methodName: String = "DELETE"

    override fun matches(
        pathSections: Stack<String>,
        requestElements: ConstantRequestElements
    ): MatchResult<Tail> {
        return if (requestElements.method == Method.DELETE) prev.matches(
            pathSections,
            requestElements
        ) else FailedToMatch()
    }
}

data class Connect<Tail : AnyParams>(
    val prev: PathRouting<Tail>
) : MethodPart<Tail>() {
    override val methodName: String = "CONNECT"

    override fun matches(
        pathSections: Stack<String>,
        requestElements: ConstantRequestElements
    ): MatchResult<Tail> {
        return if (requestElements.method == Method.CONNECT) prev.matches(
            pathSections,
            requestElements
        ) else FailedToMatch()
    }
}

data class Options<Tail : AnyParams>(
    val prev: PathRouting<Tail>
) : MethodPart<Tail>() {
    override val methodName: String = "OPTIONS"

    override fun matches(
        pathSections: Stack<String>,
        requestElements: ConstantRequestElements
    ): MatchResult<Tail> {
        return if (requestElements.method == Method.OPTIONS) prev.matches(
            pathSections,
            requestElements
        ) else FailedToMatch()
    }
}

data class Trace<Tail : AnyParams>(
    val prev: PathRouting<Tail>
) : MethodPart<Tail>() {
    override val methodName: String = "TRACE"

    override fun matches(
        pathSections: Stack<String>,
        requestElements: ConstantRequestElements
    ): MatchResult<Tail> {
        return if (requestElements.method == Method.TRACE) prev.matches(
            pathSections,
            requestElements
        ) else FailedToMatch()
    }
}

data class Post<Body, Tail : AnyParams>(
    val bodyParser: Parser<Body>,
    val prev: PathRouting<Tail>
) : MethodPart<ParamsOf<Body, Tail>>() {
    override val methodName: String = "POST"

    override fun matches(
        pathSections: Stack<String>,
        requestElements: ConstantRequestElements
    ): MatchResult<ParamsOf<Body, Tail>> {
        return if (requestElements.method == Method.POST) requestElements.body?.let(bodyParser)?.let { parseResult ->
            parseResult.fold(
                { FailedToMatch() },
                { body ->
                    val result = prev.matches(pathSections, requestElements)
                    when (result) {
                        is SuccessfulMatch<Tail> -> SuccessfulMatch(body andA result.params)
                        is FailedToMatch -> FailedToMatch()
                    }
                }
            )
        } ?: FailedToMatch() // TODO: Bodyless POSTs are perfectly legal, need to fix this
        else FailedToMatch()
    }
}

data class Put<Body, Tail : AnyParams>(
    val bodyParser: Parser<Body>,
    val prev: PathRouting<Tail>
) : MethodPart<ParamsOf<Body, Tail>>() {
    override val methodName: String = "PUT"

    override fun matches(
        pathSections: Stack<String>,
        requestElements: ConstantRequestElements
    ): MatchResult<ParamsOf<Body, Tail>> {
        return if (requestElements.method == Method.PUT) requestElements.body?.let(bodyParser)?.let { parseResult ->
            parseResult.fold(
                { FailedToMatch() },
                { body ->
                    val result = prev.matches(pathSections, requestElements)
                    when (result) {
                        is SuccessfulMatch<Tail> -> SuccessfulMatch(body andA result.params)
                        is FailedToMatch -> FailedToMatch()
                    }
                }
            )
        } ?: FailedToMatch() // TODO: Bodyless PUTs are perfectly legal, need to fix this
        else FailedToMatch()
    }
}

data class Patch<Body, Tail : AnyParams>(
    val bodyParser: Parser<Body>,
    val prev: PathRouting<Tail>
) : MethodPart<ParamsOf<Body, Tail>>() {
    override val methodName: String = "PATCH"

    override fun matches(
        pathSections: Stack<String>,
        requestElements: ConstantRequestElements
    ): MatchResult<ParamsOf<Body, Tail>> {
        return if (requestElements.method == Method.PATCH) requestElements.body?.let(bodyParser)?.let { parseResult ->
            parseResult.fold(
                { FailedToMatch() },
                { body ->
                    val result = prev.matches(pathSections, requestElements)
                    when (result) {
                        is SuccessfulMatch<Tail> -> SuccessfulMatch(body andA result.params)
                        is FailedToMatch -> FailedToMatch()
                    }
                }
            )
        } ?: FailedToMatch() // TODO: Bodyless PATCHes are perfectly legal, need to fix this
        else FailedToMatch()
    }
}

/**
 * A PathBit is a type used as part of the Routing DSL, it represents a literal part of a path
 */
data class PathBit(val pathBit: String)

/**
 * A PathParamBit is a type used as part of the Routing DSL, it represents a path parameter.
 * It requires a name for the path parameter, and a parser to turn it in to a useful domain type.
 */
data class PathParamBit<A>(val paramName: String, val transformer: Parser<A>)

/**
 * This indicates that the route only needs to match some prefix of the whole URL, before attempting to use the rest of
 *   the path to access resources from some `basePath` from the application's resources.
 */
data class StaticResourceServerBit(val basePath: String)

sealed class MethodHandler<Response>
data class BodylessMethodHandler<Response>(
    val method: Method.BodylessMethod
) : MethodHandler<Response>()

data class BodyMethodHandler<Body, Response>(
    val method: Method.BodyMethod,
    val bodyParser: Parser<Body>
) : MethodHandler<Response>()

fun path(pathBit: String) = PathBit(pathBit)
fun <A> param(paramName: String, parser: Parser<A>) = PathParamBit(paramName, parser)
fun staticResources(basePath: String) = StaticResourceServerBit(basePath)

// Handle strings as part of the path
operator fun String.div(s: String): PathRouting<HNil> = path(this) / path(s)
operator fun <A> String.div(param: PathParamBit<A>) = path(this) / param
operator fun String.div(staticResourceServer: StaticResourceServerBit) = path(this) / staticResourceServer

// Handle path bits sitting on their own
operator fun PathBit.div(nextPath: PathBit) = root / this.pathBit / path(nextPath.pathBit)
operator fun <A> PathBit.div(param: PathParamBit<A>) =
    root / path(this.pathBit) / param(param.paramName, param.transformer)

operator fun PathBit.div(staticResourceServer: StaticResourceServerBit) =
    root / path(this.pathBit) / staticResourceServer

operator fun <A, B> PathParamBit<A>.div(param: PathParamBit<B>) =
    root / param(this.paramName, this.transformer) / param(param.paramName, param.transformer)

operator fun <A : AnyParams> PathRouting<A>.div(path: String) = this / path(path)
operator fun <A : AnyParams> PathRouting<A>.div(pathBit: PathBit) = PathPart(pathBit.pathBit, this)
operator fun <A : AnyParams, B> PathRouting<A>.div(param: PathParamBit<B>) =
    PathParam(param.paramName, param.transformer, this)

operator fun <A : AnyParams> PathRouting<A>.div(staticResourceServer: StaticResourceServerBit) =
    StaticResourceServer(staticResourceServer.basePath, this)

fun <A> get(): BodylessMethodHandler<A> = BodylessMethodHandler(Method.GET)
fun <A> head(): BodylessMethodHandler<A> = BodylessMethodHandler(Method.HEAD)
fun <A> delete(): BodylessMethodHandler<A> = BodylessMethodHandler(Method.DELETE)
fun <A> connect(): BodylessMethodHandler<A> = BodylessMethodHandler(Method.CONNECT)
fun <A> options(): BodylessMethodHandler<A> = BodylessMethodHandler(Method.OPTIONS)
fun <A> trace(): BodylessMethodHandler<A> = BodylessMethodHandler(Method.TRACE)

fun <Body, Result> post(bodyParser: Parser<Body>): BodyMethodHandler<Body, Result> =
    BodyMethodHandler(Method.POST, bodyParser)

fun <Body, Result> put(bodyParser: Parser<Body>): BodyMethodHandler<Body, Result> =
    BodyMethodHandler(Method.PUT, bodyParser)

fun <Body, Result> patch(bodyParser: Parser<Body>): BodyMethodHandler<Body, Result> =
    BodyMethodHandler(Method.PATCH, bodyParser)

infix fun <A> PathBit.on(methodHandler: BodylessMethodHandler<A>) = root / this on methodHandler
infix fun <A> String.on(methodHandler: BodylessMethodHandler<A>) = path(this) on methodHandler
infix fun <Body, Response> PathBit.on(methodHandler: BodyMethodHandler<Body, Response>) = root / this on methodHandler
infix fun <Body, Response> String.on(methodHandler: BodyMethodHandler<Body, Response>) = path(this) on methodHandler

infix fun <A : AnyParams, Result> PathRouting<A>.on(methodHandler: BodylessMethodHandler<Result>): RoutingElement<A> =
    when (methodHandler.method) {
        Method.GET -> Get(this)
        Method.HEAD -> Head(this)
        Method.CONNECT -> Connect(this)
        Method.DELETE -> Delete(this)
        Method.OPTIONS -> Options(this)
        Method.TRACE -> Trace(this)
    }

infix fun <Body, A : AnyParams, Result> PathRouting<A>.on(methodHandler: BodyMethodHandler<Body, Result>): RoutingElement<ParamsOf<Body, A>> =
    when (methodHandler.method) {
        Method.POST -> Post(methodHandler.bodyParser, this)
        Method.PUT -> Put(methodHandler.bodyParser, this)
        Method.PATCH -> Patch(methodHandler.bodyParser, this)
    }

val root = PathRoot

fun printRouting(elements: RoutingElement<*>): String {
    return when (elements) {
        is PathRouting<*> -> printPath(elements)
        // Unfortunately have to match each individual method here since sometimes the type parameter for `prev` changes
        //    but not always. Could split in to body/bodyless in that hierarchy as well I suppose
        is AllMethods<*, *, *> -> "${elements.methodName} ${printPath(elements.prev)}"
        is Get<*> -> "${elements.methodName} ${printPath(elements.prev)}"
        is Connect -> "${elements.methodName} ${printPath(elements.prev)}"
        is Delete -> "${elements.methodName} ${printPath(elements.prev)}"
        is Head -> "${elements.methodName} ${printPath(elements.prev)}"
        is Options -> "${elements.methodName} ${printPath(elements.prev)}"
        is Trace -> "${elements.methodName} ${printPath(elements.prev)}"
        is StaticResourceServer<*> -> "Static fileserver ${printPath(elements.prev)}"
        is Post<*, *> -> "${elements.methodName} ${printPath(elements.prev)}"
        is Patch<*, *> -> "${elements.methodName} ${printPath(elements.prev)}"
        is Put<*, *> -> "${elements.methodName} ${printPath(elements.prev)}"
    }
}

fun <A : AnyParams> printPath(path: PathRouting<A>): String {
    // I could implement this as a conversion to a list of Strings then `joinToString`
    // I wonder which would be faster
    return when (path) {
        is PathRoot -> "/"
        // In order to render things the way a human would expect, in the non-root cases
        //   we need to consider both the current and previous elements
        is PathPart -> when (path.prev) {
            is PathRoot -> "/${path.pathPart}"
            is PathPart -> "${printPath(path.prev)}/${path.pathPart}"
            is PathParam<*, *> -> "${printPath(path.prev)}/${path.pathPart}"
        }
        is PathParam<*, *> -> when (path.prev) {
            is PathRoot -> "/:${path.paramName}"
            is PathPart -> "${printPath(path.prev)}/:${path.paramName}"
            is PathParam<*, *> -> "${printPath(path.prev)}/:${path.paramName}"
        }
    }
}

fun Route<*, *>.path(): String {
    return when (val route = this.route) {
        is PathRouting<*> -> printPath(route)
        // Unfortunately have to match each individual method here since sometimes the type parameter for `prev` changes
        //    but not always. Could split in to body/bodyless in that hierarchy as well I suppose
        is AllMethods<*, *, *> -> printPath(route.prev)
        is Get<*> -> printPath(route.prev)
        is Connect -> printPath(route.prev)
        is Delete -> printPath(route.prev)
        is Head -> printPath(route.prev)
        is Options -> printPath(route.prev)
        is Trace -> printPath(route.prev)
        is StaticResourceServer<*> -> printPath(route.prev)
        is Post<*, *> -> printPath(route.prev)
        is Patch<*, *> -> printPath(route.prev)
        is Put<*, *> -> printPath(route.prev)
    }
}

fun <A> RoutingElement<*>.foldAll(
    onAllMethods: (AllMethods<*, *, *>) -> A,
    onConnect: (Connect<*>) -> A,
    onDelete: (Delete<*>) -> A,
    onGet: (Get<*>) -> A,
    onHead: (Head<*>) -> A,
    onOptions: (Options<*>) -> A,
    onPatch: (Patch<*, *>) -> A,
    onPost: (Post<*, *>) -> A,
    onPut: (Put<*, *>) -> A,
    onTrace: (Trace<*>) -> A,
    onStaticResourceServer: (StaticResourceServer<*>) -> A,
    onPathParam: (PathParam<*, *>) -> A,
    onPathPart: (PathPart<*>) -> A,
    onPathRoot: (PathRoot) -> A
): A {
    return when (this) {
        is AllMethods<*, *, *> -> onAllMethods(this)
        is Connect -> onConnect(this)
        is Delete -> onDelete(this)
        is Get -> onGet(this)
        is Head -> onHead(this)
        is Options -> onOptions(this)
        is Patch<*, *> -> onPatch(this)
        is Post<*, *> -> onPost(this)
        is Put<*, *> -> onPut(this)
        is Trace -> onTrace(this)
        is StaticResourceServer<*> -> onStaticResourceServer(this)
        is PathParam<*, *> -> onPathParam(this)
        is PathPart -> onPathPart(this)
        is PathRoot -> onPathRoot(this)
    }
}

fun <A> RoutingElement<*>.foldAllRec(
    onAllMethods: (AllMethods<*, *, *>) -> A,
    onConnect: (Connect<*>) -> A,
    onDelete: (Delete<*>) -> A,
    onGet: (Get<*>) -> A,
    onHead: (Head<*>) -> A,
    onOptions: (Options<*>) -> A,
    onPatch: (Patch<*, *>) -> A,
    onPost: (Post<*, *>) -> A,
    onPut: (Put<*, *>) -> A,
    onTrace: (Trace<*>) -> A,
    onStaticResourceServer: (StaticResourceServer<*>) -> A,
    onPathParam: (PathParam<*, *>) -> A,
    onPathPart: (PathPart<*>) -> A,
    onPathRoot: (PathRoot) -> A,
    combine: (A, A) -> A
): A {
    fun folding(element: RoutingElement<*>): A = element.foldAllRec(
        onAllMethods,
        onConnect,
        onDelete,
        onGet,
        onHead,
        onOptions,
        onPatch,
        onPost,
        onPut,
        onTrace,
        onStaticResourceServer,
        onPathParam,
        onPathPart,
        onPathRoot,
        combine
    )

    return when (this) {
        is AllMethods<*, *, *> -> combine(onAllMethods(this), folding(this.prev))
        is Connect -> combine(onConnect(this), folding(this.prev))
        is Delete -> combine(onDelete(this), folding(this.prev))
        is Get -> combine(onGet(this), folding(this.prev))
        is Head -> combine(onHead(this), folding(this.prev))
        is Options -> combine(onOptions(this), folding(this.prev))
        is Patch<*, *> -> combine(onPatch(this), folding(this.prev))
        is Post<*, *> -> combine(onPost(this), folding(this.prev))
        is Put<*, *> -> combine(onPut(this), folding(this.prev))
        is Trace -> combine(onTrace(this), folding(this.prev))
        is StaticResourceServer<*> -> combine(onStaticResourceServer(this), folding(this.prev))
        is PathParam<*, *> -> combine(onPathParam(this), folding(this.prev))
        is PathPart -> combine(onPathPart(this), folding(this.prev))
        is PathRoot -> onPathRoot(this)
    }
}

fun <A> RoutingElement<*>.foldMethods(
    onAllMethods: (AllMethods<*, *, *>) -> A,
    onConnect: (Connect<*>) -> A,
    onDelete: (Delete<*>) -> A,
    onGet: (Get<*>) -> A,
    onHead: (Head<*>) -> A,
    onOptions: (Options<*>) -> A,
    onPatch: (Patch<*, *>) -> A,
    onPost: (Post<*, *>) -> A,
    onPut: (Put<*, *>) -> A,
    onTrace: (Trace<*>) -> A
): A? =
    this.foldAll(
        onAllMethods,
        onConnect,
        onDelete,
        onGet,
        onHead,
        onOptions,
        onPatch,
        onPost,
        onPut,
        onTrace,
        { null },
        { null },
        { null },
        { null }
    )

fun RoutingElement<*>.collectPathParams(): List<PathParam<*, *>> = foldAllRec(
    { emptyList() },
    { emptyList() },
    { emptyList() },
    { emptyList() },
    { emptyList() },
    { emptyList() },
    { emptyList() },
    { emptyList() },
    { emptyList() },
    { emptyList() },
    { emptyList() },
    { pathParam -> listOf(pathParam) },
    { emptyList() },
    { emptyList() },
    { a, b -> listOf(*a.toTypedArray(), *b.toTypedArray()) }
)

typealias Routes = List<Route<*, *>>

fun Routes.and(otherRoutes: Routes): Routes = listOf(*this.toTypedArray(), *otherRoutes.toTypedArray())
fun Routes.and(otherRoute: Route<*, *>): Routes = listOf(*this.toTypedArray(), otherRoute)

object Routing {
    fun of(vararg routes: Route<*, *>): Routes = listOf(*routes)
}

sealed class RouteMatchResult {
    abstract fun isSuccess(): Boolean
}

data class SuccessfulRouteMatch(val run: suspend () -> HttpResponse<*>, val matchedPath: String) : RouteMatchResult() {
    override fun isSuccess(): Boolean = true
}

object FailedRouteMatch : RouteMatchResult() {
    override fun isSuccess(): Boolean = false
}

// TODO: This should probably do something different/special for static file folders
data class Route<In : AnyParams, Result>(
    val route: RoutingElement<In>,
    val handler: suspend (In) -> HttpResponse<Result>
) {
    fun matches(req: HttpRequest): RouteMatchResult {
        val match = route.matches(req)
        return match.fold(
            { FailedRouteMatch },
            { p ->
                println("Matched! ${printRouting(route)}")
                println("Params are $p")
                val h = suspend { handler(p) }
                SuccessfulRouteMatch(h, this.path())
            }
        )
    }
}
