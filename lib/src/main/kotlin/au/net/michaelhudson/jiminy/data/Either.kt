package au.net.michaelhudson.jiminy.data

sealed class Either<out E, out A> {
    abstract fun <B> fold(onLeft: (E) -> B, onRight: (A) -> B): B

    abstract fun <B> map(mapper: (A) -> B): Either<E, B>

    companion object {
        fun <E, A> right(value: A) = Right<E, A>(value)
        fun <E, A> left(value: E) = Left<E, A>(value)
    }
}

data class Left<E, A>(val value: E) : Either<E, A>() {
    override fun <B> fold(onLeft: (E) -> B, onRight: (A) -> B): B = onLeft(value)
    override fun <B> map(mapper: (A) -> B): Either<E, B> = Left(value)
}

data class Right<E, A>(val value: A) : Either<E, A>() {
    override fun <B> fold(onLeft: (E) -> B, onRight: (A) -> B): B = onRight(value)
    override fun <B> map(mapper: (A) -> B): Either<E, B> = Right(mapper(value))
}

fun <E, A, B> Either<E, A>.flatMap(flatMapper: (A) -> Either<E, B>): Either<E, B> = when (this) {
    is Left -> Left(value)
    is Right -> flatMapper(value)
}