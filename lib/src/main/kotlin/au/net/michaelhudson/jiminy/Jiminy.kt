package au.net.michaelhudson.jiminy

import au.net.michaelhudson.jiminy.http.Routes
import au.net.michaelhudson.jiminy.http.server.ServerAdapter
import au.net.michaelhudson.jiminy.http.server.JiminyService

/**
 * The JiminyServer will be created by the end-user of the library to hook up their routes with
 *   their server of choice.
 */
class JiminyServer private constructor(private val routes: Routes, private val server: ServerAdapter) {
    suspend fun start(): Unit = server.start(JiminyService(routes))

    companion object {
        fun from(routes: Routes, server: ServerAdapter): JiminyServer = JiminyServer(routes, server)
    }
}
