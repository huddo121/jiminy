package au.net.michaelhudson.jiminy.data

sealed class HList<A>
object HNil : HList<Nothing>()
data class HNode<A, TAIL : HList<*>>(val head: A, val tail: TAIL) : HList<A>()

fun <A> justA(a: A): HNode<A, HNil> = HNode(a, HNil)
infix fun <A, TAIL : HList<*>> A.andA(tail: TAIL): HNode<A, TAIL> = HNode(this, tail)
infix fun <A, B> A.andA(b: B): HNode<A, HNode<B, HNil>> = HNode(this, HNode(b, HNil))

typealias HLStar = HList<*>
typealias HL0 = HNil
typealias HL1<A> = HNode<A, HNil>
typealias HL2<A, B> = HNode<A, HNode<B, HNil>>
typealias HL3<A, B, C> = HNode<A, HNode<B, HNode<C, HNil>>>
typealias HL4<A, B, C, D> = HNode<A, HNode<B, HNode<C, HNode<D, HNil>>>>
