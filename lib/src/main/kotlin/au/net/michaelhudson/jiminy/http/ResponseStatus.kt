package au.net.michaelhudson.jiminy.http

enum class ResponseStatus(val code: Int) {
    OK(200),
    BadRequest(400),
    NotFound(404)
}