package au.net.michaelhudson.jiminy.http

import java.io.File

/**
 * A HttpResult is a layer of indirection that allows for more uniform handling of responses from Jiminy.
 *
 * The immediate use case is to handle things like serving static files in a single place. Rather than every server
 *   needing to worry about how to handle a response of "serve any file from this directory", Jiminy will turn any
 *   particular request that happens to result in a file being served from a directory in to a response that serves a
 *   specific file.
 */
sealed class HttpResult
data class BodylessHttpResult(val status: ResponseStatus) : HttpResult()
data class BodyHttpResult<A>(val status: ResponseStatus, val body: A) :
    HttpResult() // TODO: How do I turn these bodies in to a wire value?

data class FileHttpResult(val status: ResponseStatus, val file: File) : HttpResult()
