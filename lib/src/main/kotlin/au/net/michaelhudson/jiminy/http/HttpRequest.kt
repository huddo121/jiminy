package au.net.michaelhudson.jiminy.http

import java.net.URI

// What should the type of `body` actually be? String? Or a type param?
// It might depend on what I'm willing to deal with in HTTP
data class HttpRequest(val uri: URI, val method: Method, val body: String? = null)