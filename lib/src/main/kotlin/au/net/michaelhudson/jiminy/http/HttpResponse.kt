package au.net.michaelhudson.jiminy.http

import java.io.File

// TODO: Better types for things
// TODO: Rethink the API around producing HttpResponses
sealed class HttpResponse<ResponseBody>(val status: ResponseStatus)
data class BodyHttpResponse<ResponseBody>(val _status: ResponseStatus, val body: ResponseBody) : HttpResponse<ResponseBody>(_status)

/**
 * Loads static files from the classpath
 * @param basePath The base path to search for files for within `resources`
 */
data class StaticFileDirectoryResponse(val basePath: String) : HttpResponse<Any>(ResponseStatus.OK)
data class FileResponse(val _status: ResponseStatus, val file: File) : HttpResponse<File>(_status)
data class BodylessHttpResponse(val _status: ResponseStatus) : HttpResponse<Nothing>(_status)
