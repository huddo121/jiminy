package au.net.michaelhudson.jiminy.http.server

import au.net.michaelhudson.jiminy.http.*
import java.io.File

/**
 * This class is how {@link ServerAdapter}s interact with Jiminy. It will take care of the Jiminy-specific behaviours
 *   that occur when dealing with a response from the user's routing and logic.
 */
data class JiminyService(private val routes: Routes) {
    suspend fun handle(request: HttpRequest): HttpResult {
        // Figure out if there's a route that can handle the incoming request
        val matchResult = routes.map { route ->
            route.matches(request)
        }.find { it.isSuccess() }

        return when (matchResult) {
            is SuccessfulRouteMatch -> {
                when (val result = matchResult.run()) {
                    is BodyHttpResponse<*> -> BodyHttpResult(result.status, result.body)
                    is BodylessHttpResponse -> BodylessHttpResult(result.status)
                    is StaticFileDirectoryResponse -> {
                        // Turn this in to a FileResponse because I don't want to do directory listing
                        // This will be something like /my/place/static/some/subdir/pics.jpg
                        val path = request.uri.path
                        // This should be what route the user of Jiminy defined to serve the directory from
                        // So something like /my/place/static
                        val matchedPath = matchResult.matchedPath

                        // Something whack is happening here and things don't _always_ work with folder structures
                        val filepath = result.basePath + path.removePrefix(matchedPath)

                        // Have to grab the class off of something, can't just get my own?
                        val resource = matchResult.javaClass.getResource(filepath)
                        val resourceAsURI = resource?.let { p -> p.toURI() }
                        val file = resourceAsURI?.let { File(it) }

                        if (file !== null && file.isFile && file.exists()) {
                            FileHttpResult(result.status, file)
                        } else {
                            BodylessHttpResult(ResponseStatus.NotFound)
                        }
                    }

                    is FileResponse -> {
                        FileHttpResult(result.status, result.file)
                    }
                }
            }

            FailedRouteMatch -> BodylessHttpResult(ResponseStatus.NotFound)
            null -> BodylessHttpResult(ResponseStatus.NotFound)
        }
    }
}