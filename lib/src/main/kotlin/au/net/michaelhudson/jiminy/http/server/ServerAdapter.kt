package au.net.michaelhudson.jiminy.http.server

// TODO: This might be the right place to thread through any adapters for HttpResult -> Bytes
abstract class ServerAdapter {
    /**
     * Starts the webserver and provides it with the routes the user wants to match on
     */
    abstract suspend fun start(jiminyHandler: JiminyService): Unit
}