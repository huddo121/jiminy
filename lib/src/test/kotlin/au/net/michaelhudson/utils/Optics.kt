package au.net.michaelhudson.utils

import arrow.optics.Lens
import au.net.michaelhudson.jiminy.http.HttpRequest
import au.net.michaelhudson.jiminy.http.Method
import java.net.URI

object URILenses {
    val path: Lens<URI, String> = Lens(
        { uri -> uri.path },
        { uri, s -> URI(uri.scheme, uri.host, s, uri.fragment) }
    )
}

object HttpRequestLenses {
    // If I was willing to have the dependency on Arrow as part of the library I could just generate these things
    val uri: Lens<HttpRequest, URI> = Lens(
        HttpRequest::uri,
        { req, uri -> req.copy(uri = uri) }
    )

    val path: Lens<HttpRequest, String> = uri compose URILenses.path

    val method: Lens<HttpRequest, Method> = Lens(
        HttpRequest::method,
        { req, method -> req.copy(method = method) }
    )

    // TODO: That focus type doesn't seem right, body shouldn't be `String?`
    val body: Lens<HttpRequest, String?> = Lens(
        HttpRequest::body,
        { req, body -> req.copy(body = body) }
    )
}
