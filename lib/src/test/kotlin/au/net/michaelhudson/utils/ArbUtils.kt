package au.net.michaelhudson.utils

import io.kotest.property.Arb
import io.kotest.property.arbitrary.flatMap
import io.kotest.property.arbitrary.map

fun <A, B> Arb<A>.zip(arbB: Arb<B>): Arb<Pair<A, B>> = this.flatMap { a -> arbB.map { b -> Pair(a, b) } }
fun <A, B, C> zip(arbA: Arb<A>, arbB: Arb<B>, arbC: Arb<C>): Arb<Triple<A, B, C>> =
    arbA.zip(arbB).flatMap { (a, b) -> arbC.map { c -> Triple(a, b, c)}}