package au.net.michaelhudson.data

import au.net.michaelhudson.jiminy.data.Either
import io.kotest.property.Arb
import io.kotest.property.arbitrary.choice
import io.kotest.property.arbitrary.map

object EitherArbitraries {
    fun <E, A> arbEither(arbE: Arb<E>, arbA: Arb<A>): Arb<Either<E, A>> =
        Arb.choice(arbE.map { Either.left(it) }, arbA.map { Either.right(it) })
}