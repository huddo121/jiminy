package au.net.michaelhudson.http

import au.net.michaelhudson.data.EitherArbitraries.arbEither
import au.net.michaelhudson.jiminy.data.justA
import au.net.michaelhudson.jiminy.http.*
import au.net.michaelhudson.utils.HttpRequestLenses
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.shouldStartWith
import io.kotest.property.Arb
import io.kotest.property.arbitrary.*
import io.kotest.property.checkAll
import java.net.URI

class RoutingTests : DescribeSpec({
    describe("Route DSL") {
        describe("constructing simple paths") {
            it("rootpath is rendered as /") {
                printPath(root) shouldBe "/"
            }
            it("render for single path element") {
                checkAll(Arb.string()) { s ->
                    printPath(root / s) shouldBe "/$s"
                }
            }
            it("any two path elements should render as /first/second") {
                checkAll(Arb.string(), Arb.string()) { a, b -> printPath(a / b) shouldBe "/$a/$b" }
            }
            it("intersperse / between each path element") {
                checkAll(Arb.list(Arb.string(), 0..10)) { items ->
                    val result = items.fold<String, PathRouting<NoParams>>(root, { s, acc -> s / acc })
                    val expectedPath = items.joinToString("/", "/")
                    printPath(result) shouldBe expectedPath
                }
            }
        }
        describe("paths with parameters") {
            it("parameters are rendered like :paramName") {
                checkAll(Arb.string()) { s ->
                    printPath(root / param(s, ::parseSuccess)) shouldBe "/:$s"
                }
            }
        }
        describe("routings with methods") {
            it("renders with the method name at the start") {
                checkAll(Arbitraries.arbPath, Arbitraries.arbMethodHandler(::parseSuccess)) { path, method ->
                    val (route, methodName) = when (method) {
                        is BodyMethodHandler<*, *> -> Pair(path on method, method.method)
                        is BodylessMethodHandler<*> -> Pair(path on method, method.method)
                    }
                    // This is a shithouse property to test, these two things aren't _really_ related
                    printRouting(route) shouldStartWith methodName.toString()
                }
            }
        }
    }
    describe("Path Matching") {
        // TODO: Should create better paths
        val arbPathFromBits: Arb<PathRouting<*>> =
            Arb.list(Arb.string(0..20, Arb.alphanumeric()).map(::path), 1..10)
                .map { it.fold(root, { acc: PathRouting<*>, bit -> acc / bit }) }

        it("root path matches for any URI with no path") {
            checkAll(HttpArbitraries.arbURI, HttpArbitraries.arbMethod) { uri, method ->
                val pathlessURI = URI(uri.scheme, uri.host, "/", "")
                val request = HttpRequest(pathlessURI, method, null)

                root.matches(request) shouldBe SuccessfulMatch(NoParams)
            }
        }

        it("basic path matches URI ") {
            checkAll(arbPathFromBits, HttpArbitraries.arbHttpRequest) { path, req ->
                val updatedRequest = HttpRequestLenses.path.set(req, printPath(path))

                path.matches(updatedRequest) shouldBe SuccessfulMatch(NoParams)
            }
        }

        describe("with params") {
            it("matches if parser succeeds") {
                checkAll(
                    Arb.string(1..20, Arb.alphanumeric()),
                    Arb.string(1..20, Arb.alphanumeric()),
                    HttpArbitraries.arbHttpRequest
                ) { paramName, paramValue, req ->
                    val path = root / param(paramName, ::parseSuccess)
                    val updatedRequest = HttpRequestLenses.path.set(req, "/$paramValue")
                    path.matches(updatedRequest) shouldBe SuccessfulMatch(justA(paramValue))
                }
            }

            it("does not match if parser fails") {
                checkAll(
                    Arb.string(1..20, Arb.alphanumeric()),
                    Arb.string(1..20, Arb.alphanumeric()),
                    HttpArbitraries.arbHttpRequest
                ) { paramName, paramValue, req ->
                    val path = root / param(paramName, { v -> parseFailure<String>("failed", v) })
                    val updatedRequest = HttpRequestLenses.path.set(req, "/$paramValue")
                    path.matches(updatedRequest) shouldBe FailedToMatch()
                }
            }
        }
    }

    describe("Method handling") {
        // TODO
    }
})

object Arbitraries {
    fun <A> arbPathParamBit(value: Arb<A>): Arb<PathParamBit<A>> =
        value.flatMap { a -> Arb.string().map { name -> param(name, { parseSuccess(a) }) } }

    val arbPathParamString: Arb<PathParamBit<String>> = arbPathParamBit(Arb.string())
    val arbPathPart: Arb<PathBit> = Arb.string().map(::path)
    val arbPath: Arb<PathRouting<out AnyParams>> =
        Arb.list(arbEither(arbPathPart, arbPathParamString), 1..10).map { items ->
            items.fold(
                root,
                { acc: PathRouting<*>, part -> part.fold({ acc / it }, { acc / it }) }
            )
        }

    fun <A> arbMethodHandler(bodyParser: Parser<A>): Arb<MethodHandler<A>> = Arb.choice(
        Arb.constant(get()),
        Arb.constant(post(bodyParser))
    )
}
