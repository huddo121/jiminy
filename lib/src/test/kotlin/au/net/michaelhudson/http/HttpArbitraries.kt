package au.net.michaelhudson.http

import au.net.michaelhudson.jiminy.http.HttpRequest
import au.net.michaelhudson.jiminy.http.Method
import au.net.michaelhudson.utils.zip
import io.kotest.property.Arb
import io.kotest.property.arbitrary.*
import java.net.URI

object HttpArbitraries {
    private val arbScheme: Arb<String> = Arb.choice(Arb.constant("http"), Arb.constant("https"))
    private val arbHost: Arb<String> =
        Arb.list(Arb.element((('a'..'z')).toList()), 1..4).map { parts -> parts.joinToString(".") }
    private val arbPath: Arb<String> =
        Arb.list(Arb.element((('a'..'z') + ('0'..'9')).toList()), 1..4).map { parts -> "/" + parts.joinToString("/") }
    val arbURI: Arb<URI> = zip(arbScheme, arbHost, arbPath).map { (scheme, host, path) -> URI(scheme, host, path, "") }

    val arbMethod: Arb<Method> = Arb.choice(
        Arb.constant(Method.GET),
        Arb.constant(Method.HEAD),
        Arb.constant(Method.DELETE),
        Arb.constant(Method.CONNECT),
        Arb.constant(Method.OPTIONS),
        Arb.constant(Method.TRACE),
        Arb.constant(Method.POST),
        Arb.constant(Method.PUT),
        Arb.constant(Method.PATCH)
    )

    // TODO: Add Body
    val arbHttpRequest: Arb<HttpRequest> = arbURI.zip(arbMethod).map { (uri, method) -> HttpRequest(uri, method, null) }
}