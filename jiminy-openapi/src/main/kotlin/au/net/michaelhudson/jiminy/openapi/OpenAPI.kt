package au.net.michaelhudson.jiminy.openapi

import au.net.michaelhudson.jiminy.http.*
import cc.vileda.openapi.dsl.asJson
import cc.vileda.openapi.dsl.openapiDsl
import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.Operation
import io.swagger.v3.oas.models.PathItem
import io.swagger.v3.oas.models.Paths
import io.swagger.v3.oas.models.parameters.Parameter
import io.swagger.v3.oas.models.parameters.PathParameter
import org.json.JSONObject

object OpenAPI {
    fun specFromRoutes(routes: Routes): OpenAPI {
        return openapiDsl {
            paths(
                routes.toPaths()
            )
        }
    }

    fun withSpecRoute(route: Route<*, *>): Routes = withSpecRoute(listOf(route))

    fun withSpecRoute(routes: Routes): Routes {
        val spec = specFromRoutes(routes)
        val swaggerRoute =
            Route("swagger" / "spec.json" on get<JSONObject>()) {
                BodyHttpResponse(
                    ResponseStatus.OK,
                    spec.asJson().toString(2)
                )
            }

        return listOf(*routes.toTypedArray(), swaggerRoute)
    }

    fun withSwaggerUi(route: Route<*, *>): Routes = withSwaggerUi(listOf(route))

    fun withSwaggerUi(routes: Routes): Routes {
        val swaggerUiRoute =
            Route("swagger" / staticResources("/swagger-ui")) { StaticFileDirectoryResponse("/swagger-ui") }

        return listOf(*withSpecRoute(routes).toTypedArray(), swaggerUiRoute)
    }
}

fun Routes.toPaths(): Paths {
    // Disgusting mutability
    val paths = Paths()

    // Need to group the routes by path, operations need to exist on the same PathItem for a given route
    val pathGroups = this.groupBy { r -> r.toOpenApiPath() }

    // Turn each group in to a PathItem
    val pathItems: Map<String, PathItem> = pathGroups.mapValues { entry ->
        with(PathItem()) {
            entry.value.forEach { route ->
                route.route.foldMethods(
                    {},
                    {},
                    { delete = route.toOperation() },
                    { get = route.toOperation() },
                    {},
                    {},
                    { patch = route.toOperation() },
                    { post = route.toOperation() },
                    { put = route.toOperation() },
                    {}
                )
            }
            this
        }
    }

    pathItems.forEach { entry -> paths.addPathItem(entry.key, entry.value) }
    return paths
}

fun Route<*, *>.toOperation(): Operation = with(Operation()) {
    val route = this@toOperation
    summary = "Summary toOperation - ${route.toOpenApiPath()}"
    parameters = route.route.toParameterList()
    return this
}

// Oh nice, the objects supplied don't enforce the invariants mentioned in the spec...
// What good design.
// https://spec.openapis.org/oas/latest.html#parameter-object
// TODO: Currently only cares about path params
fun <ParamsList : AnyParams> RoutingElement<ParamsList>.toParameterList(): List<Parameter> {
    val pathParams = this.collectPathParams()
//    println("pathParams - ${pathParams.map { p -> p.paramName }}")

    val pathParameterList: List<PathParameter> = pathParams.map { param ->
        val p = PathParameter()
        p.name = param.paramName
        p
    }

    return pathParameterList
}

// Copy-pastad this to use the correct templating format for OpenApi3
// https://spec.openapis.org/oas/latest.html#path-templating
fun Route<*, *>.toOpenApiPath(): String {
    fun <A : AnyParams> printPath(path: PathRouting<A>): String {
        return when (path) {
            is PathRoot -> "/"
            // In order to render things the way a human would expect, in the non-root cases
            //   we need to consider both the current and previous elements
            is PathPart -> when (path.prev) {
                is PathRoot -> "/${path.pathPart}"
                is PathPart -> "${printPath(path.prev)}/${path.pathPart}"
                is PathParam<*, *> -> "${printPath(path.prev)}/${path.pathPart}"
            }
            is PathParam<*, *> -> when (path.prev) {
                is PathRoot -> "/{${path.paramName}}"
                is PathPart -> "${printPath(path.prev)}/{${path.paramName}}"
                is PathParam<*, *> -> "${printPath(path.prev)}/{${path.paramName}}"
            }
        }
    }

    fun Route<*, *>.path(): String {
        return when (val route = this.route) {
            is PathRouting<*> -> printPath(route)
            // Unfortunately have to match each individual method here since sometimes the type parameter for `prev` changes
            //    but not always. Could split in to body/bodyless in that hierarchy as well I suppose
            is AllMethods<*, *, *> -> printPath(route.prev)
            is Get<*> -> printPath(route.prev)
            is Connect -> printPath(route.prev)
            is Delete -> printPath(route.prev)
            is Head -> printPath(route.prev)
            is Options -> printPath(route.prev)
            is Trace -> printPath(route.prev)
            is StaticResourceServer<*> -> printPath(route.prev)
            is Post<*, *> -> printPath(route.prev)
            is Patch<*, *> -> printPath(route.prev)
            is Put<*, *> -> printPath(route.prev)
        }
    }

    return this@toOpenApiPath.path()
}
