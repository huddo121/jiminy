# The Design of Jiminy 🦗

The core idea of Jiminy is that a web server should be modeled as a function that takes a request, and return a
response. This response will occur asynchronously.

This leads to the server having a shape roughly like `Request -> Future<Response>`.

## Design Options

I'm not entirely sure what I'd want the API surface area of Jiminy to look like, or how opinionated I want the library
to be.

### [Akka-Http](https://doc.akka.io/docs/akka-http/current/introduction.html#philosophy)

For the most part, I don't mind Akka-Http's APIs. It gives you a lot of tools for essentially constructing your own
ad-hoc web framework. That does mean that it's a little bit harder to start with, but eventually you will end up with
something that suits your needs. The rejection model is a little funky at first, specifically around cancellations, so
you have to be careful about defining what your route will accept so that you don't shadow any other important
rejections that you're expecting to consume closer to the edge of your app.

### [Servant](https://docs.servant.dev/en/stable/)

Servant's big goal is to define APIs as types, and then using that to drive implementations. Being able to promote and
demote between the term/type levels is pretty crucial to the idea, and I don't think I could go as far as they do in
Kotlin. Defining your APIs as a type gives you a nice ability to ensure that a specific URI accepts and returns data of
specific shapes, `Input -> m Result`. This also means you can represent a client in the same way you represent a server.
I really like the ability to ensure a route's response is a specific type, and it would be nice to be able to generate
documentation for a Jiminy server that is verifiable at compile time, rather than using something outside of the core
language like annotations.

### [http4k](https://www.http4k.org/)

It's *almost* exactly what I'd want in a web server, only it doesn't do async. Its core concept is the same, and its
idea of `Filter` matches pretty close to what I had in my head for Jiminy. The way they do
[OpenAPI3](https://www.http4k.org/guide/modules/contracts/) routes doesn't seem all that great, and automated docs is
something I've very keen on.

### What to do for Jiminy?

To summarise the things I desire:

* Simple model and representation of a web server
* Typesafe routing
* Automated docs generated from the types
* Composable routes

Without the ability in kotlin to promote/demote between types and terms, I think I'm stuck with having to be more
opinionated on how you can structure your web server in order to meet my other goals.

## Jiminy APIs

One option is to very much mimic Akka-Http. Here a request would be travelling down the routes until it came to this
specific route, at which point it would be checked to see if it matched the filters (in this case just `path`). The
`withRequest` function is essentially a way to get access to the actual request object so that information could be
extracted from it.

```kotlin
val usersRoute = path("/users") {
    withRequest { request -> respondJson(Person("Mikey", 29)) }
}
```

One of the nice things about Akka-Http is that the `Directive`s can be composed together, and whether or not a directive
makes some data available is tracked such that the function you put after the directives just makes sense.

```scala
def route = (requireAuthenticationHeader & path("/details") & getClientIp) { (authHeader, clientId) =>
  ...
}
```

Another option is somewhat more sinatra-like in that routes are seemingly only allowed to be defined in a single level
of structure. The whole `namespace` thing seems more limited than allowing arbitrary nested routes, as I think it only
handles URI prefixing (and providing a closure to dump utils in).

```kotlin
// JsonRoute<Input, ResponseBody>
val usersRoute: JsonRoute<Nothing, Array<User>> = TODO()
val picturesRoute: JsonRoute<Search, Array<Picture>> = TODO()
val routes = Routes(
    "/users" to usersRoute,
    "/pictures" to picturesRoute
)
```

In order to actually work it would require some language for defining paths that would maintain the path params, and
also the desired body shape. I'm not sure how I could represent this in a sane way.

```kotlin
// Exposing an API like this would be great, but what is the resulting type?
val path = pathSection<UserId>("userId") and ("/repositories") pathSection < UUID >("repoId")
```

Perhaps I would have to come up with some way for these combinators to construct larger and larger tuples that would
eventually result in something like a `PathProviding<(A, B, C)>`. How can I glue this together with the route handlers
though? Perhaps they need to actually construct something like `Handler<PathParams, Body, Result>`?

I think that could work fairly well, at the cost of some flexibility in how you can construct routes.

Since a lot of routes are going to have common prefixes, maybe the way things could work would be that the server
expects to be given a list of `(PathRouting, Handler)`, and that if you have 3 endpoints with a common prefix you could
be given a utility that takes a `List<(PathRouting, Handler)>` and multiplies it by the prefix.

```kotlin
val routes = List.of(
    "users" / param("userId", ::asUserId) getsTo getUserById,
    "repositories" / param("repoId", ::asRepoId) getsTo getRepoById,
    "projects" / param("projectId", ::asProjectID) getsTo getProjectById,
)

val apiRoutes = ("gateway" / "api") prefixes routes
// Results in the same thing as
List.of(
    "gateway" / "api" / "users" / param("userId", ::asUserId) getsTo getUserById,
    "gateway" / "api" / "repositories" / param("repoId", ::asRepoId) getsTo getRepoById,
    "gateway" / "api" / "projects" / param("projectId", ::asProjectID) getsTo getProjectById,
)
```

Although looking at the above I spot some problems;

1. I'm going to need a Heterogenous List if I am tracking the handler inputs in the types
2. I can't prefix things if I am demanding that the parameters supplied by the paths and those required by the handlers
   line up.
