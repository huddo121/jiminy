# Servers and adapter design

Since Jiminy currently only handles routing, there's no reason it has to be married to any specific server. Indeed,
__not__ being limited in such a way is actually a design goal here.

## High-level overview

There are three main parts to the server handling;

* `JiminyServer`
    * Constructed by the end-user of the library, glues together the routing and the other server pieces
* `ServerAdapter`s
    * Allows different servers (eg. Netty) to defer request routing responsibilities to Jiminy
* `JiminyService`
    * Only interacted with by `ServerAdapter`s, handles common logic for certain types of responses, like serving files
      from directories

### An adpater's job

Adapters are how individual server implementations defer work to the routing of Jiminy. When an HTTP Request comes in
the Adapter is responsible for turning it in to a `HttpRequest` object and making sure all the fields of the object have
sensible values places in them, and then passing that `HttpRequest` to Jiminy. Once Jiminy has routed and handled the
actual request, the Adapter must also then convert the response from a `HttpResult` in to whatever format the server
implementation desires.

That's really it, just a little bit of data transformation and some function calls.