package au.net.michaelhudson.jiminy.examples

import au.net.michaelhudson.jiminy.JiminyServer
import au.net.michaelhudson.jiminy.examples.OpenApiExample.Domain.parseAccountId
import au.net.michaelhudson.jiminy.http.*
import au.net.michaelhudson.jiminy.netty.NettyAdapter
import au.net.michaelhudson.jiminy.netty.NettyAdapterOptions
import au.net.michaelhudson.jiminy.openapi.OpenAPI
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.launch
import java.util.*
import java.util.concurrent.Executors

println("Starting OpenApiExample script")

// Creating a CoroutineScope for our server to run in
val coroutineScope = CoroutineScope(Executors.newFixedThreadPool(2).asCoroutineDispatcher())

// Set up a simple route
fun parseInt(value: String) = value.toIntOrNull()?.let(::parseSuccess) ?: parseFailure("cannot parse int", value)
val examplePath = "double" / param("number", ::parseInt)
val exampleRoute =
    Route(
        examplePath on get<String>(),
        handler { number -> BodyHttpResponse<String>(ResponseStatus.OK, "$number * 2 = ${number * 2}") }
    )

object Domain {

    @JvmInline
    value class AccountId(val id: String)

    fun parseAccountId(value: String): ParseResult<AccountId> = parseSuccess(AccountId(value))

    @JvmInline
    value class Email(val email: String)
    data class User(val accountId: AccountId, val email: Email, val displayName: String)
}

val accountUrl = "v1" / "Users" / param("userId", ::parseAccountId)

val userParser: Parser<Domain.User> = { TODO() }

val getUserRoute = "v1" / "Users" / param("userId", ::parseAccountId) on get<Domain.User>()

val getUser = Route(getUserRoute) { (accountId) ->
    println("getting user with id $accountId")
    BodyHttpResponse(
        ResponseStatus.OK,
        Domain.User(accountId, Domain.Email("hugh.mann@example.com"), "Hugh Mann")
    )
}

// Really this should take in a UserPatch type
val updateUser = Route(
    accountUrl on patch<Domain.User, Domain.User>(userParser),
    handler { user, accountId ->
        println("updating user with id $accountId")
        BodyHttpResponse(ResponseStatus.OK, Domain.User(accountId, user.email, user.displayName))
    }
)

val createUser = Route(
    "v1" / "Users" on post<Domain.User, Domain.User>(userParser),
    handler { user ->
        println("creating user - $user")
        BodyHttpResponse(
            ResponseStatus.OK,
            Domain.User(
                Domain.AccountId(
                    UUID.randomUUID().toString()
                ),
                user.email,
                user.displayName
            )
        )
    }
)

val userApi = Routing.of(getUser, updateUser, createUser)

// This will expose an OpenApi3 spec at /swagger/spec.json and swagger-ui at /swagger/index.html
val withSwaggerUi = OpenAPI.withSwaggerUi(userApi.and(exampleRoute))

// We're going to use Netty as our webserver
val nettyAdapter = NettyAdapter(NettyAdapterOptions(8000), coroutineScope)
coroutineScope.launch {
    JiminyServer.from(withSwaggerUi, nettyAdapter).start()
}
println("OpenApiExample started...")
