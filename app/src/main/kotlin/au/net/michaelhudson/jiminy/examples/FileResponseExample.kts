package au.net.michaelhudson.jiminy.examples

import au.net.michaelhudson.jiminy.JiminyServer
import au.net.michaelhudson.jiminy.http.*
import au.net.michaelhudson.jiminy.netty.NettyAdapter
import au.net.michaelhudson.jiminy.netty.NettyAdapterOptions
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.launch
import java.io.File
import java.util.concurrent.Executors

println("Starting FileResponseExample script")

// Creating a CoroutineScope for our server to run in
val coroutineScope = CoroutineScope(Executors.newFixedThreadPool(2).asCoroutineDispatcher())

// Small util for loading files
fun loadFile(path: String): File = File(coroutineScope.javaClass.classLoader.getResource(path).toURI())

// Set up a simple route
val exampleRoute =
    Route(root) {
        FileResponse(ResponseStatus.OK, loadFile("index.html"))
    }
val pictureRoute = Route("welcome.jpg" on get<File>()) {
    FileResponse(ResponseStatus.OK, loadFile("welcome.jpg"))
}

// Serve up all files within the following resource directory
val directoryRoute = Route("static" / staticResources("/")) {
    // The fact that this has a handler is somewhat useless.
    // Currently the resource basePath will be taken from this response, but I don't like that, it's gotta go!
    StaticFileDirectoryResponse("/")
}

// We're going to use Netty as our webserver
val nettyAdapter = NettyAdapter(NettyAdapterOptions(8000), coroutineScope)
coroutineScope.launch {
    JiminyServer.from(listOf(directoryRoute, exampleRoute, pictureRoute), nettyAdapter)
}