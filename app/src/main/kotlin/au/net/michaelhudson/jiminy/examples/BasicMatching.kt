package au.net.michaelhudson.jiminy.examples

import au.net.michaelhudson.jiminy.http.*
import kotlinx.coroutines.runBlocking
import java.net.URI

object BasicMatching {
    // All webservers will be adapted to provide a common HttpRequest object
    val simpleGetRequest = HttpRequest(URI("http://localhost/example"), Method.GET, null)
    val simplePostRequest = HttpRequest(URI("http://localhost/example"), Method.POST, null)

    // RoutingElements are created using the Routing DSL. These are used to determine which function should handle a
    //  given HttpRequest
    val simpleGetPath = "example" on get<String>() // At the example route, on GET, we will respond with a string

    // Routes are how you provide the handler for requests that match the given RoutingElements
    val simpleGetRoute = Route(simpleGetPath) { BodyHttpResponse(ResponseStatus.OK, "hello!") }

    // You can easily include path parameters as part of your RoutingElements
    // Params need to be parsed in to richer domain objects, for this example we'll accept any String.
    val simplePathWithParam = "users" / param("userId", ::parseSuccess) on get<String>()

    // If you look at the type of simplePathWithParam you'll see it's a `RoutingElement<ParamsOf<String, NoParams>>`.
    // You can read this as "This RoutingElement needs to be handled by a function that takes a String, and nothing else."
    val simpleRouteWithParam =
        Route(simplePathWithParam, handler { userId -> BodyHttpResponse(ResponseStatus.OK, "The user id is $userId") })

    // At runtime when handling HTTP requests Jiminy will find which route will handle the request and run the matching
    //  handler.
    // You are not expected to write code like the below, this is effectively what Jiminy does for you
    val simpleResult = runBlocking {
        val matchResult = simpleRouteWithParam.matches(HttpRequest(URI("/users/12345678"), Method.GET))
        when (matchResult) {
            FailedRouteMatch -> BodylessHttpResponse(ResponseStatus.NotFound)
            is SuccessfulRouteMatch -> matchResult.run()
        }
    }

    // We can of course route requests with multiple path parameters, see Domain below for details on these Elements.
    // If you look at the type of `moreComplexPath` you can see that we are going to have a RepoId and a UserId available
    //  to us whenever we handle a request that matches these RoutingElements.
    val moreComplexPath: RoutingElement<TwoParams<Domain.RepoId, Domain.UserId>> =
        "users" / Domain.userId / "repositories" / Domain.repoId on get<Map<String, String>>()

    // The type signature annotated above is a type alias covering up the real type, which is somewhat harder to read.
    // The inferred type is going to be `RoutingElement<ParamsOf<Domain.RepoId, ParamsOf<Domain.UserId, NoParams>>>`.
    // This shows a little too much of the machinery of how Jiminy works, which is to accumulate the parameters to your
    //  eventual route handler as a chain of type parameters. The `handler` function acts as the bridge between this
    //  tricky representation and a function that accepts simple parameters.

    // When we're constructing our `Route` we will be gain access to all the parsed path params. If our path changes
    //  and no longer parses out certain objects (or if it parses an extra one), then our code will no longer compile.
    // The Kotlin compiler will force us to make sure that
    val moreComplexRoute = Route(
        moreComplexPath,
        handler { repoId, userId ->
            // Since `moreComplexPath` had both a `UserId` and `RepoId` being parsed, we have access to them in our handler.
            BodyHttpResponse(ResponseStatus.OK, mapOf("repoId" to repoId.value, "userId" to userId.id))
        }
    )

    val result = runBlocking {
        val matchResult =
            moreComplexRoute.matches(HttpRequest(URI("/users/12345678/repositories/1234567890ABCDEF"), Method.GET))
        when (matchResult) {
            FailedRouteMatch -> BodylessHttpResponse(ResponseStatus.NotFound)
            is SuccessfulRouteMatch -> matchResult.run()
        }
    }
}

// These are the domain types that our application deals with
object Domain {
    // We create these as abstract classes because we want to ensure that they're only created from the provided helper
    //  functions. This means that if we ever have a `UserId` object, we _know_ it must have come from here, and has
    //  passed all required validation.
    abstract class UserId(val id: String)

    // The `userId` helper function (sometimes called a 'smart constructor') will only produce a `UserId` object if it
    //  passes our validation rules.
    fun userId(id: String): UserId? = if (id.length == 8) object : UserId(id) {} else null

    // Our `userIdParser` is responsible for transforming URI path parameters in to our domain objects.
    // If the path parameter doesn't meet our validation rules, we'll fail the parse and
    val userIdParser: Parser<UserId> =
        { value -> userId(value)?.let(::parseSuccess) ?: parseFailure("Couldn't parse UserId", value) }

    // This just wraps up all of the above ideas in an easily reusable fashion. This can be handy when you have very
    //  common path parameters that you use often in your application
    val userId = param("userId", userIdParser)

    // RepoId is much the same as UserId, only the validation rules are different.
    // Since RepoId and UserId aren't just raw strings floating around in our application, we can't pass one in to a
    //  function that would expect the other. And since you can only get a `RepoId` object from here, we know that all
    //  `RepoId` objects are valid!
    abstract class RepoId(val value: String)

    private fun repoId(value: String): RepoId? = if (value.length == 16) object : RepoId(value) {} else null
    val repoIdParser: Parser<RepoId> =
        { value -> repoId(value)?.let(::parseSuccess) ?: parseFailure("Couldn't parse RepoId", value) }
    val repoId = param("repoId", repoIdParser)

    // These are example values, you shouldn't do this in real code, since we're circumventing the protections we put
    //  in above!
    val exampleUserId: UserId = object : UserId("12345678") {}
    val exampleRepoId: RepoId = object : RepoId("1234567890ABCDEF") {}
}
