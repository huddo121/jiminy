import au.net.michaelhudson.jiminy.JiminyServer
import au.net.michaelhudson.jiminy.http.*
import au.net.michaelhudson.jiminy.netty.NettyAdapter
import au.net.michaelhudson.jiminy.netty.NettyAdapterOptions
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.launch
import java.util.concurrent.Executors

// Creating a CoroutineScope for our server to run in
val coroutineScope = CoroutineScope(Executors.newFixedThreadPool(2).asCoroutineDispatcher())

// Set up a simple route
fun parseInt(value: String) = value.toIntOrNull()?.let(::parseSuccess) ?: parseFailure("cannot parse int", value)
val examplePath = "double" / param("number", ::parseInt) on get<String>()
val exampleRoute =
    Route(examplePath, handler { number -> BodyHttpResponse(ResponseStatus.OK, "$number * 2 = ${number * 2}") })

// We're going to use Netty as our webserver
val nettyAdapter = NettyAdapter(NettyAdapterOptions(8000), coroutineScope)

val server = JiminyServer.from(listOf(exampleRoute), nettyAdapter)

coroutineScope.launch {
    server.start()
}
println("Netty server started...")