package au.net.michaelhudson.jiminy.netty

import au.net.michaelhudson.jiminy.http.*
import au.net.michaelhudson.jiminy.http.server.ServerAdapter
import au.net.michaelhudson.jiminy.http.server.JiminyService
import io.netty.bootstrap.ServerBootstrap
import io.netty.buffer.Unpooled
import io.netty.channel.*
import io.netty.channel.nio.NioEventLoopGroup
import io.netty.channel.socket.SocketChannel
import io.netty.channel.socket.nio.NioServerSocketChannel
import io.netty.handler.codec.http.*
import io.netty.handler.codec.http.HttpHeaderNames.CONTENT_TYPE
import io.netty.handler.codec.http.HttpRequest
import io.netty.handler.codec.http.HttpVersion.HTTP_1_1
import io.netty.util.CharsetUtil
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import java.io.RandomAccessFile
import java.net.URI

data class NettyAdapterOptions(val port: Int)

class NettyAdapter(private val options: NettyAdapterOptions, private val scope: CoroutineScope) : ServerAdapter() {

    override suspend fun start(jiminyHandler: JiminyService) {
        val parentGroup = NioEventLoopGroup()
        val childGroup = NioEventLoopGroup()

        val handler = object : ChannelInitializer<SocketChannel>() {
            override fun initChannel(ch: SocketChannel) {
                ch.pipeline()
                    .addLast(HttpRequestDecoder())
                    .addLast(HttpResponseEncoder())
                    .addLast(NettyHandler(jiminyHandler, scope))
            }
        }

        val server = ServerBootstrap().group(parentGroup, childGroup)
            .channel(NioServerSocketChannel::class.java)
            .childHandler(handler)
            .option(ChannelOption.SO_BACKLOG, 128)
            .childOption(ChannelOption.SO_KEEPALIVE, true)

        server.bind(options.port)
    }
}

fun HttpMethod.toJiminy(): Method = when (this) {
    HttpMethod.GET -> Method.GET
    HttpMethod.POST -> Method.POST
    // It's a dodgy catch-all but it's a catch-all
    else -> Method.GET
}

fun ResponseStatus.toNettyStatus(): HttpResponseStatus = HttpResponseStatus.valueOf(this.code)

fun toResponse(httpResult: HttpResult): DefaultFullHttpResponse {
    return when (httpResult) {
        is BodylessHttpResult -> DefaultFullHttpResponse(
            HTTP_1_1,
            httpResult.status.toNettyStatus()
        )

        is BodyHttpResult<*> -> DefaultFullHttpResponse(
            HTTP_1_1,
            httpResult.status.toNettyStatus(),
            Unpooled.copiedBuffer(httpResult.body.toString(), CharsetUtil.UTF_8)
        )
        // The FileResponse-ish result from Jiminy should include important things like the MIME-type
        is FileHttpResult -> {
            val file = httpResult.file
            val byteBuf = Unpooled.copiedBuffer(file.readBytes())
            val response = DefaultFullHttpResponse(HTTP_1_1, httpResult.status.toNettyStatus(), byteBuf)

            // I'm just stealing code from Netty at this point, not sure why these things are done but I trust them more
            //   than I trust me so in to the code soup it goes
            // See https://netty.io/4.0/xref/io/netty/example/http/file/HttpStaticFileServerHandler.html
            val raf: RandomAccessFile = RandomAccessFile(file, "r")
            // TODO: Jiminy should be doing this garbage
            val contentType: String = when (file.extension) {
                "jpg" -> "image/jpeg"
                "jpeg" -> "image/jpeg"
                "html" -> "text/html"
                else -> "unknown"
            }
            response.headers().set(CONTENT_TYPE, contentType)
            HttpUtil.setContentLength(response, raf.length())
            response
        }
    }
}

class NettyHandler(private val jiminyHandler: JiminyService, private val scope: CoroutineScope) :
    SimpleChannelInboundHandler<Any>() {

    // TODO: Read up on Netty, I'm assuming that making these mutable actually makes this unsafe, unless the main group
    //         always only ever accepts a single HttpRequest at a time... Doubt it though.
    private var method: Method? = null
    private var uri: URI? = null
    private var req: HttpRequest? = null

    override fun channelReadComplete(ctx: ChannelHandlerContext?) {
        ctx?.flush()
    }

    // TODO: I bet this pattern match is pretty dodgy
    override fun channelRead0(ctx: ChannelHandlerContext?, msg: Any?) {
        when (msg) {
            is HttpRequest -> {
                if (HttpUtil.is100ContinueExpected(msg) && ctx != null) writeResponse(ctx)

                method = msg.method().toJiminy()
                uri = URI(msg.uri())
                req = msg
            }

            is LastHttpContent -> {
                val content = msg.content().toString(CharsetUtil.UTF_8)

                req?.let { r ->
                    val httpRequest =
                        au.net.michaelhudson.jiminy.http.HttpRequest(URI(r.uri()), r.method().toJiminy(), content)

                    scope.launch {
                        val resp = jiminyHandler.handle(httpRequest)
                        respond(ctx!!, r, resp)
                    }
                }
            }

            else -> {
                println("Couldn't match on channelRead0 - ${msg?.javaClass?.name ?: "No msg"}:$msg")
            }
        }
    }

    // TODO: Expose default handlers for 500s
    override fun exceptionCaught(ctx: ChannelHandlerContext?, cause: Throwable?) {
        println("${cause?.message}")
        ctx?.close()
    }

    private fun writeResponse(ctx: ChannelHandlerContext) {
        val response: FullHttpResponse = DefaultFullHttpResponse(
            HTTP_1_1,
            HttpResponseStatus.CONTINUE,
            Unpooled.EMPTY_BUFFER
        )
        ctx.write(response)
    }

    private fun respond(ctx: ChannelHandlerContext, nettyRequest: HttpRequest, httpResponse: HttpResult) {
        val keepAlive = HttpUtil.isKeepAlive(nettyRequest)

        val response = toResponse(httpResponse)

        if (keepAlive) {
            response.headers().setInt(HttpHeaderNames.CONTENT_LENGTH, response.content().readableBytes())
            response.headers().set(HttpHeaderNames.CONNECTION, HttpHeaderValues.KEEP_ALIVE)
        }

        ctx.write(response)
        ctx.flush()

        if (!keepAlive) {
            ctx.writeAndFlush(Unpooled.EMPTY_BUFFER).addListener(ChannelFutureListener.CLOSE)
        }
    }
}
